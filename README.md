## suguri tracker

[![pipeline status](https://gitlab.com/aos2/aos2.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/aos2/aos2.gitlab.io/commits/master)

Client-side web application that tracks events and matches for the [Acceleration of Suguri 2 community][aos2] by querying [our GraphQL backend][sumikat]. Components are written with [Svelte][svelte] while [three.js] is used to render a 3D background.

Full application: https://aos2.gitlab.io/

Lite version: https://aos2.gitlab.io/lite.html (no 3D background)

## requirements

* [node] for development

## development

Install development tools with `npm install --only=dev` which includes a pre-commit hook that checks for errors reported by [prettier] and [eslint]. Use `npm run lint` to automatically adjust files in-place. Serve the site locally with `npm run dev` and the app hotreloads when you make changes affecting `/public` output.

[aos2]: https://aos2.gitlab.io
[eslint]: https://eslint.org/
[node]: https://nodejs.org/en/
[prettier]: https://prettier.io/
[sumikat]: https://gitlab.com/aos2/sumikat
[svelte]: https://svelte.dev/
[three.js]: https://threejs.org/
