module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: ["standard", "prettier", "prettier/standard"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: ["prettier", "svelte3"],
  overrides: [
    {
      files: ["*.svelte"],
      rules: {
        "import/first": "off",
        "import/no-duplicates": "off",
        "import/no-mutable-exports": "off",
        "prettier/prettier": "off"
      }
    }
  ],
  rules: {
    "prettier/prettier": "error"
  }
};
