import Lite from "./src/Lite.svelte";

const app = new Lite({
  target: document.body
});

export default app;
