import { writable } from "svelte/store";
import Community from "./Community.svelte";
import Events from "./Events.svelte";
import Matches from "./Matches.svelte";

const defaultFilter = {
  char1: "",
  char2: "",
  name1: "",
  name2: "",
  slot1_win: "",
  before_date: "",
  after_date: "",
  round: "",
  order: "desc",
  version: "",
  limit: "10",
  offset: 0
};

function createFilter() {
  const { subscribe, set, update } = writable(Object.assign({}, defaultFilter));
  return {
    subscribe,
    set: newFilter => set(newFilter),
    reset: () => set(Object.assign({}, defaultFilter)),
    update
  };
}

function createView() {
  const { subscribe, set } = writable(Events);
  return {
    subscribe,
    set: view => {
      if (Object.keys(views).includes(view)) {
        set(views[view]);
      }
    },
    updateView: () => {
      const anchor = window.location.href.split("#")[1].toLowerCase();
      if (anchor) view.set(anchor);
    }
  };
}

export const filter = createFilter();
export const matches = writable([]);
export const matchesStatuses = {
  inProgress: "inProgress"
};
export const meta = writable({
  cached: false,
  dates: [],
  names: [],
  versions: []
});
export const completed = writable([]);
export const upcoming = writable([]);
export const view = createView();
export const views = {
  matches: Matches,
  events: Events,
  community: Community
};
