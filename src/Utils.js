import { addMinutes } from "date-fns";
import { gql } from "../secret.json";

export function calibrateDate(date) {
  date = new Date(Number(date));
  return addMinutes(date, date.getTimezoneOffset());
}

/*
 * Wrapper to make a GraphQL query to our backend,
 * returning the response we're interested in.
 * query: GraphQL query function
 */
export async function fetch(query) {
  const resp = await window
    .fetch(gql, {
      body: query(),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      mode: "cors"
    })
    .then(r => {
      if (!r.ok) throw r;
      return r.json();
    })
    .catch(e => {
      e.text().then(t => {
        console.error(t, "\n", query());
      });
    });
  return resp.data;
}

/*
 * Return a string str with string end removed from its end.
 * str: String
 * end: String
 */
export function strip(str, end) {
  return str.endsWith(end) ? str.slice(0, -1 * end.length) : str;
}

/*
 * Return a string str with string start removed from its beginning.
 * str: String
 * end: String
 */
export function stripStart(str, start) {
  return str.startsWith(start) ? str.slice(start.length) : str;
}
